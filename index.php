<!--- Yep --->
<?php
if(!isset($_SESSION))
{
session_start();
}

//get the variables
$uid = $_GET["uid"];
$pathName = $_GET["pathName"];

if(!isset($_SESSION['onTour'])){
	$_SESSION['onTour'] = false;
}
$onTour = $_SESSION['onTour'];

include 'database.php';

?>

<?php
// Load doctype
require_once('common/doctype.html');
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US">
<?php 
//htmlHeader HTML
require_once('common/HTMLheader.php');
?>
<body>

<div id = "wrap">
<?php
//Header HTML
require_once('common/header.html');
?>
	<div id = "content">
		<div id = "main" >
			<p>
			<?php
				
				//We need to determine if they have scanned a qr code
				if($uid == null and $pathName == null) { require_once('noQR.php');}
				elseif ($uid != null) {
					
					// If we have a QR code, lets parse it
					// Remember our format:
					// uid = B_name
					// uid = R_num
					// uid = I_serial

					$QR = explode("_", $uid);
					
					//figure out what to query
					if($QR[0] == 'B'){
						$building = getBuilding(null,$QR[1]);
						$a = parseBuilding($building);
						echo $a;
						echo updatePrompt($uid);
						/*
						if($onTour){
							$b = getNext(null,$uid);
							echo $b;
						}
						elseif(!$onTour){
							//Here is where we would include code to determine
							//if the building is the begining of a path or not. 
							//for the demo I am assuming the uilding begins the tour
							$b = getNext(null,$uid);
							echo $b;
							$_SESSION['onTour'] = true;
						}
						*/
					}
					elseif($QR[0] == 'R'){
						$room = getRoom(null,$QR[1]);
						$r = parseRoom($room);
						echo $r;
						
						/* for later
						if($onTour) {
							// get the items in the room
							$items = getNext(null,$uid);
							echo $uid;
							echo $items;
						}
						if(!onTour) {
							$a = startTourPrompt($uid);
							echo $a;
						}
						*/
						
					}
					elseif($QR[0] == 'I'){
						$item = getItem(null,$QR[1]);
						$temp = updateItemInspect($uid);  // update item's last inspection date
						$i = parseItem($item);
						echo $i;
						echo updatePrompt($uid);
						echo wrongLocationPrompt($uid);
						/*s
						if($onTour) {
							// get the items in the room
							$items = getItemsInRoom($uid);
							echo $items;
						}
						if(!onTour) {
							$a = startTourPrompt($uid);
						}
						*/
					}
					elseif($QR[0] == 'II'){
						// TODO
						$wall = getItemIdent($QR[1],null);
						$a = parseItemIdent($wall);
						echo $a;
						echo missingPrompt($uid);
						
					}
					else { echo "<h2>Invalid QR code, please scan again</h2>";};
				}
				elseif ($pathName != null) {
					//if we have a pathname, but no UID, display the first station 
					$nextQR = getNext($pathName);
					echo $nextQR;
				}
				
				if(isset($uid)) {
					//get the next QR code to scan
					$items = getNext(null,$uid);
					echo $items;
				}
				
				require_once('common/scan.php');
				$button = drawScanButton();
				echo $button;
			?>
			</p>
		</div>
		
		<div id="side">
			<div id="sidea">
				<?php require_once('common/sideA.php'); ?>
			</div>
			<div id="sideb">
				<?php require_once('common/sideB.php'); ?>
			</div>
			<div id="sidec">
				<?php require_once('common/sideC.php'); ?>
			</div>
		</div>
		
	</div> <!-- close content-->
	<?php //load footer
	// load closing files
	require_once('common/footer.html'); 
	?>
	
</div> <!-- close Wrap--->

</body>
</html>