<?php
/*
 * File containing Database functions
 * This is the old/more general backup of database.php
 *
 */

 define("DB_HOST", "localhost");
 define("DB_ID", "k11943_Dev");
 define("DB_USER", "k11943_Dev");
 define("DB_PASSWORD", "4d4b296e32f7c");
 
 function connectDB(){
    $link = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
    if (!$link) {
      die('Could not connect: ' . mysql_error());
    }
    mysql_select_db(DB_ID) or die(mysql_error());
  }
  
function closeDB(){
    mysql_close($link);
  }
  
function getBuilding($id_building=null, $name=null){
    connectDB();
	if($id_building == null) {
		$result = mysql_query("SELECT * FROM building WHERE building.name = '".$name."'")
			or die("GETBUILDING".mysql_error());
		$building = mysql_fetch_array( $result );
	}
	elseif($name == null) {
		$result = mysql_query("SELECT * FROM building WHERE id_building = '".$id_building."'")
			or die("GETBUILDING".mysql_error());
		$building = mysql_fetch_array( $result );
	}
	else {
		echo "failed to do anything useful";
		/*
		$result = mysql_query("SELECT * FROM building WHERE id_building = ".$id_building." and name = ".$name)
			or die("GETBUILDING".mysql_error());
		$building = mysql_fetch_array( $result );
		*/
	}
	closeDB();
    return $building;
  }
  
function getRoom($id_room=null, $num_room=null){
	connectDB();
	if($id_room == null) {
		$result = mysql_query("SELECT * FROM room WHERE num_room = '".$num_room."'")
			or die("GETRoom".mysql_error());
		$room = mysql_fetch_array( $result );
	}
	elseif($num_room == null) {
		$result = mysql_query("SELECT * FROM room WHERE id_room = '".$id_room."'")
			or die("GETRoom".mysql_error());
		$room = mysql_fetch_array( $result );
	}
	else {
		$result = mysql_query("SELECT * FROM room WHERE id_room = '".$id_room."' and num_room = '".$num_room."'")
			or die("GETRoom".mysql_error());
		$room = mysql_fetch_array( $result );
	}
	closeDB();
    return $room;
  }

function getItem($id_item=null, $num_serial=null, $room_id_room=null, $room_num_room = null, $room_building_id_building=null, $room_building_name=null){
    //function to get item based on PKs
	//note: unfinished, there should be a better way to ignore null parameters
	connectDB();
	if($id_item == null) {
		$result = mysql_query("SELECT * FROM item WHERE num_serial = '".$num_serial."'")
			or die("GETRoom".mysql_error());
		$item = mysql_fetch_array( $result );
	}
	elseif($num_serial == null) {
		$result = mysql_query("SELECT * FROM item WHERE id_item = '".$id_item."'")
			or die("GETRoom".mysql_error());
		$item = mysql_fetch_array( $result );
	}
	elseif($room_num_room != null) {
		$result = mysql_query("SELECT * FROM item WHERE room_num_room = '".room_num_room."'")
			or die("GETRoom".mysql_error());
		$item = mysql_fetch_array( $result );
	}
	else {
		$result = mysql_query("SELECT * FROM item WHERE id_item = '".$id_item."' and num_serial = '".$num_serial."'")
			or die("GETRoom".mysql_error());
		$item = mysql_fetch_array( $result );
	}
	closeDB();
    return $item;
  }
  
function parseBuilding($buildingIn){
	// List where to begin
	// DD column values:
	// [id_building],[name],[date_built],[description],[notes]
	
	$id_building = $buildingIn['id_building'];
	$name        = $buildingIn['name'];
	$date_built  = $buildingIn['date_built'];
	$description = $buildingIn['description'];
	$notes       = $buildingIn['notes'];
	
	$head = "<h1>".$name."</h1>";
	$table = "
				<br />
				<table id = \"buildingTable\" cellspacing = \"0\" >
				<tr> 
					<th scope = \"col\" >Building ID</th>
					<th scope = \"col\" >Building Name</th>
					<th scope = \"col\" >Build Date</th>
					<th scope = \"col\" >Description</th>
					<th scope = \"col\" >Notes</th>
				</tr>
				<tr>
					<td scope = \"row\" class = \"spec\">".$id_building."</td>
					<td>".$name."</td>
					<td>".$date_built."</td>
					<td>".$description."</td>
					<td>".$notes."</td>
				</tr>
				</table>";
	
	$building = $head."\n".$table."<br />";
	return $building;
}

function parseRoom($roomIn){
	// Get Rooom
	// Get Item(s) to scan (and what order)
	// Return large string to print in Main
	
	// room table columns
	// [id_room],[num_room],[directions],[description],[notes],[building_id_building],[building_name]
	
	
	$id_room              = $roomIn['id_room'];
	$num_room             = $roomIn['num_room'];
	$directions           = $roomIn['directions'];
	$description          = $roomIn['description'];
	$notes                = $roomIn['notes'];
	$building_id_building = $roomIn['building_id_building'];
	$building_name        = $roomIn['building_name'];
	
	$head = "<h1>".$building_name." : ".$num_room."</h1>";
	$table = "
				<br />
				<table id = \"roomTable\" cellspacing = \"0\" >
				<tr> 
					<th scope = \"col\" >Room ID</th>
					<th scope = \"col\" >Room Number</th>
					<th scope = \"col\" >Directions</th>
					<th scope = \"col\" >Description</th>
					<th scope = \"col\" >Notes</th>
					<th scope = \"col\" >Building</th>
				</tr>
				<tr>
					<td scope = \"row\" class = \"spec\">".$id_room."</td>
					<td>".$num_room."</td>
					<td>".$directions."</td>
					<td>".$description."</td>
					<td>".$notes."</td>
					<td>".$building_name."</td>
				</tr>
				</table>";
	
	$room = $head."\n".$table."<br />";
	
	//Lets list all the other items in the room
	$items = parseItems($num_room);
	
	
	return $room."<br />".$items;
}

function parseItem($itemIn){
	// function that runs when we scan an item QR code
	// columns:
	// [id_item],[num_serial],[type],[priority],[description],[notes],[condition],[date_manufactured],[inspect_interval],[date_last_inspect],[last_inspect_person],[size],[room_id_room],[room_num_room],[room_building_id_building],[room_building_name]
	
	$id_item             = $itemIn['id_item'];
	$num_serial          = $itemIn['num_serial'];
	$type                = $itemIn['type'];
	$priority            = $itemIn['priority'];
	$description         = $itemIn['description'];
	$notes               = $itemIn['notes'];
	$condition           = $itemIn['condition'];
	$date_manufactured   = $itemIn['date_manufactured'];
	$inspect_interval    = $itemIn['inspect_interval'];
	$date_last_inspect   = $itemIn['date_last_inspect'];
	$last_inspect_person = $itemIn['last_inspect_person'];
	$size                = $itemIn['size'];
	$room                = $itemIn['room_num_room'];
	$building            = $itemIn['room_building_name'];
	
	$head = "<h1>Building : ".$building."</h1>
				<h2>Room : ".$room."</h2>";
	
	$table = "
				<br />
				<table id = \"itemsTable\" cellspacing = \"0\" >
				<tr> 
					<th scope = \"col\" >Item ID</th>
					<th scope = \"col\" >Serial Number</th>
					<th scope = \"col\" >Type</th>
					<th scope = \"col\" >priority</th>
					<th scope = \"col\" >Description</th>
					<th scope = \"col\" >Notes</th>
					<th scope = \"col\" >Condition</th>
					<th scope = \"col\" >Date Manufactured</th>
					<th scope = \"col\" >Inspect Interval</th>
					<th scope = \"col\" >Date Last Inspected</th>
					<th scope = \"col\" >Last Inspector</th>
					<th scope = \"col\" >size</th>
				</tr>
				<tr>
					<td scope = \"row\" class = \"spec\">".$id_item."</td>
					<td>".$num_serial."</td>
					<td>".$type."</td>
					<td>".$priority."</td>
					<td>".$description."</td>
					<td>".$notes."</td>
					<td>".$condition."</td>
					<td>".$date_manufactured."</td>
					<td>".$inspect_interval."</td>
					<td>".$date_last_inspect."</td>
					<td>".$last_inspect_person."</td>
					<td>".$size."</td
				</tr>
				</table>";
				
	return $head."<hr />".$table;
}

function parseItems($num_room){
	// function to parse a list of items
	// columns
	// [id_item],[num_serial],[type],[priority],[description],[notes],[condition],[date_manufactured],[inspect_interval],[date_last_inspect],[last_inspect_person],[size],[room_id_room],[room_num_room],[room_building_id_building],[room_building_name]
	
	$table = "
				<br />
				<table id = \"itemsTable\" cellspacing = \"0\" >
				<tr> 
					<th scope = \"col\" >Item ID</th>
					<th scope = \"col\" >Serial Number</th>
					<th scope = \"col\" >Type</th>
					<th scope = \"col\" >priority</th>
					<th scope = \"col\" >Description</th>
					<th scope = \"col\" >Notes</th>
					<th scope = \"col\" >Condition</th>
					<th scope = \"col\" >Date Manufactured</th>
					<th scope = \"col\" >Inspect Interval</th>
					<th scope = \"col\" >Date Last Inspected</th>
					<th scope = \"col\" >Last Inspector</th>
					<th scope = \"col\" >size</th>
				</tr>";
				
	connectDB();
	
	$result = mysql_query("SELECT * FROM item WHERE room_num_room = '".$num_room."'")
			or die("GETRoom".mysql_error());
			
	while ($row = mysql_fetch_assoc($result)) {
		$id_item             = $row['id_item'];
		$num_serial          = $row['num_serial'];
		$type                = $row['type'];
		$priority            = $row['priority'];
		$description         = $row['description'];
		$notes               = $row['notes'];
		$condition           = $row['condition'];
		$date_manufactured   = $row['date_manufactured'];
		$inspect_interval    = $row['inspect_interval'];
		$date_last_inspect   = $row['date_last_inspect'];
		$last_inspect_person = $row['last_inspect_person'];
		$size                = $row['size'];
		
		$table .= "
				 <tr>
					<td scope = \"row\" class = \"spec\">".$id_item."</td>
					<td>".$num_serial."</td>
					<td>".$type."</td>
					<td>".$priority."</td>
					<td>".$description."</td>
					<td>".$notes."</td>
					<td>".$condition."</td>
					<td>".$date_manufactured."</td>
					<td>".$inspect_interval."</td>
					<td>".$date_last_inspect."</td>
					<td>".$last_inspect_person."</td>
					<td>".$size."</td
				</tr>";
	}
	
	$table .= "</table>";	
	
	closeDB();
				
	return $table."<br />";
}
?>