<?php
// Load doctype
require_once('common/doctype.html');
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US">
<?php 
//htmlHeader HTML
require_once('common/HTMLheader.php');
?>
<body>

<div id = "wrap">
<?php
//Header HTML
require_once('common/header.html');
?>
	<div id = "content">
		<div id = "main" >
			<h1> Sample QR codes </h1>
			<br />
			
			<h2>Building</h2>
			<img src="http://qrcode.kaywa.com/img.php?s=6&d=http%3A%2F%2Fcsc337.gregkrol.kodingen.com%3Fuid%3DB_SCNCE" alt="qrcode"  />
			<br />
			<hr />
			
			<h2>Room 137</h2>
			<img src="http://qrcode.kaywa.com/img.php?s=6&d=http%3A%2F%2Fcsc337.gregkrol.kodingen.com%3Fuid%3DR_137" alt="qrcode"  />
			<br />
			<hr />
			
			<h2>Wall 11111</h2>
			<img src="http://qrcode.kaywa.com/img.php?s=6&d=http%3A%2F%2Fcsc337.gregkrol.kodingen.com%3Fuid%3DII_1" alt="qrcode"  />
			<br />
			<hr />
			
			<h2>Item 11111</h2>
			<img src="http://qrcode.kaywa.com/img.php?s=6&d=http%3A%2F%2Fcsc337.gregkrol.kodingen.com%3Fuid%3DI_11111" alt="qrcode"  />
			<br />
			<hr />
					
			<h2>Wall 11112</h2>
			<img src="http://qrcode.kaywa.com/img.php?s=6&d=http%3A%2F%2Fcsc337.gregkrol.kodingen.com%3Fuid%3DII_2" alt="qrcode"  />
			<br />
			<hr />
			
			<h2>Item 11112</h2>
			<img src="http://qrcode.kaywa.com/img.php?s=6&d=http%3A%2F%2Fcsc337.gregkrol.kodingen.com%3Fuid%3DI_11112" alt="qrcode"  />
			<br />
			<hr />
						
			<h2>Room 139</h2>
			<img src="http://qrcode.kaywa.com/img.php?s=6&d=http%3A%2F%2Fcsc337.gregkrol.kodingen.com%3Fuid%3DR_139" alt="qrcode"  />
			<br />
			<hr />
			
			<h2>Room 138</h2>
			<img src="http://qrcode.kaywa.com/img.php?s=6&d=http%3A%2F%2Fcsc337.gregkrol.kodingen.com%3Fuid%3DR_138" alt="qrcode"  />
			<br />
			<hr />
		</div>
		
		<div id="side">
			<div id="sidea">
				<!-- Side A content start -->
				<a href = "index.php"><h2>Home</h2></a>
				<!-- Side A content end -->
			</div>
		</div>
		
	</div> <!-- close content-->
	<?php //load footer
	// load closing files
	require_once('common/footer.html'); 
	?>
	
</div> <!-- close Wrap--->

</body>
</html>