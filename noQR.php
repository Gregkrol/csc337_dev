<?php
/*
 * This page will be dedicated to the simple task of  welcoming a new user. 
 * If we are given a pathName, get the first item of that path. 
 * otherwise, prompt the user to select a path and post to the index
 */
 
 
 
 function drawPathSelectForm($options) {
	// We create and return a string that is a form.
	$page = "
			<form action = \"index.php\" method = \"get\">
				<h2> Select Route : </h2>
				<select name = \"pathName\">
					";
	foreach ($options as $value) {
		$page .= "<option>".$value."</option>";
	}
	
	$page .= "
			</select>
				<a href = \"#\" class = \"button orange\"  onclick=\"parentNode.submit()\" >Submit</a>
			</form>";
	return $page;
 }
?>

<?php
require_once('database.php');
$a = drawPathSelectForm(getPathNames());
echo $a."
		<br />
		<br />
		";
?>