
<head>
	<title>
		csc337 QR-code project
	</title>
	<?php //Dynamically load php 
	/* 
	
	- no longer needed, just keep iPad css for now (looks fine on a regular browser)
	
	$isiPad = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPad');
	if (!$isiPad){ echo "<LINK href=\"css/base.css\" rel=\"stylesheet\" type=\"text/css\">"; }
	else { echo "<LINK href=\"css/iPad.css\" rel=\"stylesheet\" type=\"text/css\">";}
	echo $isIpad;
	echo $_SERVER['HTTP_USER_AGENT'];
	*/
	echo "<LINK href=\"/css/iPad.css\" rel=\"stylesheet\" type=\"text/css\">";
	?>

	<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
	<meta name="description" content="ECSU public safety web app demo" />
	<meta name="viewport" content="width=768px, minimum-scale=1.0, maximum-scale=1.0" />
	
	<!--Javascript function that opens on the 'iPhone, iPad etc.' QuickMark QR scanner application in the scan room page-->
    <script type="text/javascript">
        function ios_scan() {
            setTimeout(function () { }, 25);
            window.location = "quickmark://";
        }
    </script>
    
    <!--Javascript to launch android scanner-->
    <script type="text/javascript">
    	function droid_scan() {
    		window.location = "http://zxing.appspot.com/scan";
    	}
    </script>
    
    <script type="text/javascript">
    	function ios_test_scan() {
    		setTimeout(function () { }, 25);
            window.location = "scan://";
    	}
    </script>
	
</head>
