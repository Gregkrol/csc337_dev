<?php 
 /*
  * php file to update a record
  */
  
  function drawForm($row) {
	/*
	 * purpose: to create a form for us so we dont have to create a form ourselves
	 * 
	 */
	 
	$form = "
			<form name=\"updateRecord\" action=\"processUpdate.php\" method=\"get\">";
			
	$table = "
			<table id = \"updateRecordTable\" >
				 <tr>
				 ";
					
	// Populate the table header
	$row = stripRes($row);
	$keys = array_keys($row);
	
	
	foreach ($keys as &$value) {
		$tHead .= "<th scope = \"col\">".$value."</td>
				   ";
	}
	
	//P opulate values
	foreach ($row as &$value) {
		$tBody .= "<td><input type = \"text\" id = \"txtWeight\" size = \"5\" value = \"".$value."\" /></td>
				   ";
	}	
	
	$table .= $tHead;
	
	$table .= "</tr>
			   <tr>
			   ";
			   
	$table .= $tBody;
	
	$table .= "
				</tr>
			</table>
			<br />
			<a href = \"#\" class = \"button orange\"  onclick=\"parentNode.submit()\" >Submit</a>
			";
	
	$form .= $table;
	$form .= "</form>";
	
	return $form;
}

function stripRes($array) {
	// a mysql fetch_array returns useless values
	$numItems = count($array);
	$toDel = ($numItems / 2) + 1;
	
	for ($i = 0; $i < $toDel; $i++) {
		unset($array[strval($i)]);
	}
	
	unset($array['id_room']);
	unset($array['building_id_building']);
	unset($array['building_name']);
	unset($array['id_item']);
	unset($array['num_serial']);
	unset($array['id_building']);
	unset($array['room_id_room']);
	unset($array['room_num_room']);
	unset($array['room_building_id_building']);
	unset($array['room_building_name']);
	
	return $array;
}


?>

<!--- Yep --->
<?php
if(!isset($_SESSION))
{
	session_start();
}

//get the variable
$uid = $_GET["uid"];

include 'database.php';

?>

<?php
// Load doctype
require_once('common/doctype.html');
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US">
<?php 
//htmlHeader HTML
require_once('common/HTMLheader.php');
?>
<body>

<div id = "wrap">
<?php
//Header HTML
require_once('common/header.html');
?>
	<div id = "content">
		<div id = "main" >
			<p>
				<?php 
					/* fuck it, inline php 
					 * This section both gets the current record, and creates a form that allows
					 * us to edit that record.
					 * FUNCTIONS:
					 *	drawForm($attributes) - feed it an array from a mysql result
					 *							it returns a string to echo
					 * 
					 */
					
					require_once('database.php');
					
					if ($uid != null) {
						$row = parseUID($uid, true);
						
						echo "<h1>".$row['building_name']." Building</h1><br />
						";
						
						if($row == null) { echo "<h1>No such record</h1>"; }
						$table = drawForm($row);
						echo $table;
					}
					else {
						$page = "
						<form>
							<h2> Select Building </h2>
							<select>
								<option></option>
								<option>Science</option>
								<option>Webb</option>
							</select>
							<br />
							<h2> Select Room</h2>
							<select>
								<option></option>
								<option>101</option>
								<option>102</option>
								<option>103</option>
								<option>104</option>
							</select>
							<br />
							<h2>Select Item</h2>
							<select>
								<option></option>
								<option>EXT: 123</option>
								<option>EXT: 423</option>
								<option>EXT: 234</option>
							</select>
							<a href = \"recordupdate.php?uid=R_101\" class = \"button orange\" >Submit</a>
						</form>";
						echo $page;
					}
				?>
			</p>
		</div>
		
		<div id="side">
			<div id="sidea">
				<?php require_once('common/sideA.php'); ?>
			</div>
			<div id="sideb">
				<?php require_once('common/sideB.php'); ?>
			</div>
			<div id="sidec">
				<?php require_once('common/sideC.php'); ?>
			</div>
		</div>
		
	</div> <!-- close content-->
	<?php //load footer
	require_once('common/footer.html'); 
	?>
	
</div> <!-- close Wrap--->

</body>
</html>


